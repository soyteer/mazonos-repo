# Mazon OS Repository

This is a custom repo for me to simply add packages and then pull down from the internet easily using a script `nana` and `hana`.


# NANA

[Nana](./nana) is the original version of the script that downloads the packages from this repo.

# ANA

[Ana](./ana) is the package manager that ended up being the main branch of those 3 variants (ana, nana and hana). It actually pulls down the packages from the main repository of [Mazon OS](http://mazonos.com/packages/) and it even has a option to ignore packages (since the official repo has duplicated packages).

# HANA

[Hana](./hana) is the one that unifies both nana and ana, but i don't really recommend using it because the code is a it messy and under certain circumstances might break so keep that in mind if you use it.

# Installing

You can simply download any one of those and place it in your PATH that will it's job.

Here's the commands available

```
$ ana -h
:: Usage ana [ARGUMENT] [PACKAGES]
    [-c, clean]                 Clear the local cache
    [-h, help]                  Show this message
    [-i, install PACKAGES]      Install packages remotely
    [-L, list-installed]        List installed packages
    [-l, list-remote]           List remote packages
    [-r, remove PACKAGES]       Remove packages installed
    [-s, search PACKAGE]        Search for a package remotely
    [-u, update]                Update remote list
    [-U, upgrade]               TODO! Install new versions of installed packages
    [-v, version]               Show ana version
```
